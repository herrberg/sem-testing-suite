from utils import TESTS_FOLDER
from os import listdir
from os.path import isfile, join
from collections import defaultdict
import argparse

def parse_argv():
    parser = argparse.ArgumentParser(description='Create latex tables.')
    parser.add_argument('--range', nargs=2, type=int, default=(0, 15))
    parser.add_argument('--methods', type=str, default='MLW,GLS,ULS')
    return parser.parse_args()


args = parse_argv()
start, end = args.range
methods = args.methods.split(',')
head = '\\begin{table}\n\\begin{tabular}{|c|c|c|c|c|c|c|c|}\n'
head += '\\hline Pkg/OF & N & Mean-OF & Median OF & Mean $\\epsilon$ & Median $\\epsilon$ & Time (s) \\\\ \\hline\n'
tail = '\end{{tabular}}\n\label{{set{}}}\n\caption{{Set {}}}\n\end{{table}}'
for i in range(start, end):
    s = head
    for method in methods:
        filename = TESTS_FOLDER + '/set{}_{}.txt'.format(i, method)
        with open(filename, 'r') as f:
            f.readline()
            line = f.readline()
            while line.strip():
                s += line
                line = f.readline()
    s += tail.format(i + 1, i + 1)
    with open(TESTS_FOLDER + '/table{}.txt'.format(i), 'w') as f:
        f.write(s)
        
    
    
    